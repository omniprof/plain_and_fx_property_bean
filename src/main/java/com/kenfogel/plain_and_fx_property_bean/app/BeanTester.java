package com.kenfogel.plain_and_fx_property_bean.app;

// Required for Arrays.sort
import com.kenfogel.plain_and_fx_property_bean.data.Book;
import com.kenfogel.plain_and_fx_property_bean.data.BookPageComparator;
import java.time.LocalDate;
import java.util.Arrays;
// Required by the Comparator function
import static java.util.Comparator.comparing;

public class BeanTester {

    /**
     * Here is where I am testing my comparisons
     *
     */
    public void perform() {
        // Lets create four books
        Book b0 = new Book("200", "Xenon", "Hamilton", "Harcourt", 99, LocalDate.of(2017, 2, 4));
        Book b1 = new Book("500", "Boron", "Bradbury", "Prentice", 108, LocalDate.of(2018, 5, 23));
        Book b2 = new Book("300", "Radon", "Heinlein", "Thompson", 98, LocalDate.of(2015, 8, 30));
        Book b3 = new Book("404", "Argon", "Campbell", "Hachette", 102, LocalDate.of(2012, 11, 15));

        // Using Comparable to compare two books
        System.out.println("Value returned by Comparable");
        System.out.println(b0.getTitle() + " compared to " + b1.getTitle() + ": "
                + b0.compareTo(b1));
        System.out.println();

        // Using Comparator to compare two books
        System.out.println("Value returned by Comparator");
        BookPageComparator bookPageComparator = new BookPageComparator();
        System.out.println(b0.getPages() + " compared to " + b1.getPages() + ": "
                + bookPageComparator.compare(b0, b1));
        System.out.println();

        // Create an array we can sort
        Book[] myBooks = new Book[4];
        myBooks[0] = b0;
        myBooks[1] = b1;
        myBooks[2] = b2;
        myBooks[3] = b3;
        System.out.println("Unsorted");
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable Interface on Title");
        Arrays.sort(myBooks); // uses the Comparable compareTo in the bean
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable Object on Pages");
        Arrays.sort(myBooks, bookPageComparator); // uses the Comparator object
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable lambda expression on Publishers");
        Arrays.sort(myBooks, (s1, s2) -> {
            return s1.getPublisher().compareTo(s2.getPublisher());
        }); // uses the Comparator lambda
        displayBooks(myBooks);

        System.out.println("Sorted with Comparable lambda functions based on ISBN");
        Arrays.sort(myBooks, comparing(Book::getIsbn)); // Comparable function
        displayBooks(myBooks);
    }

    /**
     * Print the contents of each Book object in the array
     *
     * @param theBooks
     */
    private void displayBooks(Book[] theBooks) {
        for (Book b : theBooks) {
            System.out.print(b.getIsbn() + "\t");
            System.out.print(b.getTitle() + "\t");
            System.out.print(b.getAuthor() + "\t");
            System.out.print(b.getPublisher() + "\t");
            System.out.print(b.getPages() + "\t");
            System.out.println(b.getDatePublished());
        }
        System.out.println();
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        BeanTester bt = new BeanTester();
        bt.perform();
        System.exit(0);
    }
}
